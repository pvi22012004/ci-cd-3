﻿namespace Delivery.Api.Enums
{
    public enum OrderStatus
    {
        InProcess,
        Delivered
    }
}
