﻿namespace Delivery.Api.Classes
{
    public class Response
    {
        public string? status { get; set; }

        public string? message { get; set; }
    }
}
